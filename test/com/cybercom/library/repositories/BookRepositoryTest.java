package com.cybercom.library.repositories;

import com.cybercom.library.models.Book;
import org.junit.Test;
import static org.junit.Assert.*;

public class BookRepositoryTest {
    
    public BookRepositoryTest() {
    }
    
    @Test
    public void newRepositoryHasNoBooks() {
        
        BookRepository repo = new BookRepository();
        
        assertEquals(0, repo.all().size());
    }
    
    @Test
    public void repositoryContainsBookAfterAdd() {
        
        BookRepository repo = new BookRepository();
        Book book = new Book();
        
        repo.add(book);
        
        assertTrue(repo.all().contains(book));
    }
    
    @Test
    public void bookGetsIdAfterAddToRepository() {
        
        BookRepository repo = new BookRepository();
        Book book = new Book();
        int oldId = book.getId();
        
        repo.add(book);
        
        assertNotEquals(oldId, book.getId());
    }
    
    @Test
    public void bookGetsUniqueIdAfterAddToRepository() {
        
        BookRepository repo = new BookRepository();
        Book book1 = new Book();
        Book book2 = new Book();
        
        repo.add(book1);
        repo.add(book2);
        
        assertNotEquals(book1.getId(), book2.getId());
    }
    
    @Test
    public void bookCanBeFoundBasedOnId() {
        
        BookRepository repo = new BookRepository();
        Book book1 = new Book();
        Book book2 = new Book();
        
        repo.add(book1);
        repo.add(book2);
        
        assertSame(book1, repo.get(book1.getId()));
        assertSame(book2, repo.get(book2.getId()));
    }
    
    @Test
    public void returnsNullIfBookWithIdNotFound() {
        
        BookRepository repo = new BookRepository();
        
        assertNull(repo.get(2));
    }
}
