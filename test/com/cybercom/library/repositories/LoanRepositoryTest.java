package com.cybercom.library.repositories;

import com.cybercom.library.models.Book;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.models.Loan;
import java.util.GregorianCalendar;
import static org.junit.Assert.*;
import org.junit.Test;

public class LoanRepositoryTest {
    
    private GregorianCalendar now = new GregorianCalendar();

    @Test
    public void newRepositoryHasNoLoans() {
        
        LoanRepository repo = new LoanRepository();
        
        assertEquals(0, repo.all().size());
    }
    
    @Test
    public void repositoryContainsLoanAfterAdd() {
        
        LoanRepository repo = new LoanRepository();
        
        Loan loan = new Loan(new Book(), new Borrower(), now);
        
        repo.add(loan);
        
        assertTrue(repo.all().contains(loan));
    }
    
    @Test
    public void loanCanBeFoundBasedOnBook() {
        
        LoanRepository repo = new LoanRepository();
        Book book = new Book();
        
        book.setId(3);
        
        Loan loan = new Loan(book, new Borrower(), now);
        
        repo.add(loan);
        
        Loan actual = repo.find(book);
        
        assertSame(loan, actual);
    }
    
    @Test
    public void findReturnsNullIfNoLoanOfBookExists() {
        
        LoanRepository repo = new LoanRepository();
        Book book = new Book();
        Loan loan = new Loan(new Book(), new Borrower(), now);
        
        repo.add(loan);
        
        assertNull(repo.find(book));
    }
    
    @Test
    public void findReturnsEmptyListIfBorrowerHasNoLoans() {
        
        LoanRepository repo = new LoanRepository();
        
        assertEquals(0, repo.find(new Borrower()).size());
    }
    
    @Test
    public void findReturnsAllOfTheBorrowersLoans() {
        
        LoanRepository repo = new LoanRepository();
        Borrower borrower = new Borrower();
        
        repo.add(new Loan(new Book(), borrower, now));
        repo.add(new Loan(new Book(), borrower, now));
        
        assertEquals(2, repo.find(borrower).size());
    }
    
    @Test
    public void findOnlyReturnsTheBorrowersLoans() {
        
        LoanRepository repo = new LoanRepository();
        Borrower borrower1 = new Borrower();
        Borrower borrower2 = new Borrower();
        
        repo.add(new Loan(new Book(), borrower1, now));
        repo.add(new Loan(new Book(), borrower2, now));
        
        assertEquals(1, repo.find(borrower1).size());
        assertEquals(borrower1, repo.find(borrower1).get(0).getBorrower());
    }
    
    @Test
    public void loanCanBeRemoved() {
        
        LoanRepository repo = new LoanRepository();
        Book book = new Book();
        
        book.setId(3);
        
        Loan loan = new Loan(book, new Borrower(), now);
        
        repo.add(loan);
        repo.remove(loan);
        
        Loan actual = repo.find(book);
        
        assertNull(actual);
    }
}
