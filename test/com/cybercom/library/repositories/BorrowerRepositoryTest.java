package com.cybercom.library.repositories;

import com.cybercom.library.models.Borrower;
import org.junit.Test;
import static org.junit.Assert.*;

public class BorrowerRepositoryTest {

    @Test
    public void newRepositoryHasNoBorrowers() {
        
        BorrowerRepository repo = new BorrowerRepository();
        
        assertEquals(0, repo.all().size());
    }
    
    @Test
    public void repositoryContainsBookAfterAdd() {
        
        BorrowerRepository repo = new BorrowerRepository();
        Borrower borrower = new Borrower();
        
        repo.add(borrower);
        
        assertTrue(repo.all().contains(borrower));
    }
    
    @Test
    public void bookGetsIdAfterAddToRepository() {
        
        BorrowerRepository repo = new BorrowerRepository();
        Borrower borrower = new Borrower();
        int oldId = borrower.getId();
        
        repo.add(borrower);
        
        assertNotEquals(oldId, borrower.getId());
    }
    
    @Test
    public void bookGetsUniqueIdAfterAddToRepository() {
        
        BorrowerRepository repo = new BorrowerRepository();
        Borrower borrower1 = new Borrower();
        Borrower borrower2 = new Borrower();
        
        repo.add(borrower1);
        repo.add(borrower2);
        
        assertNotEquals(borrower1.getId(), borrower2.getId());
    }
    
    @Test
    public void bookCanBeFoundBasedOnId() {
        
        BorrowerRepository repo = new BorrowerRepository();
        Borrower borrower1 = new Borrower();
        Borrower borrower2 = new Borrower();
        
        repo.add(borrower1);
        repo.add(borrower2);
        
        assertSame(borrower1, repo.get(borrower1.getId()));
        assertSame(borrower2, repo.get(borrower2.getId()));
    }
    
    @Test
    public void returnsNullIfBookWithIdNotFound() {
        
        BorrowerRepository repo = new BorrowerRepository();
        
        assertNull(repo.get(2));
    }
}
