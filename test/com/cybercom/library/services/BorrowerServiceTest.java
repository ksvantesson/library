package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.repositories.BorrowerRepository;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BorrowerServiceTest {
    
    private BorrowerRepository borrowerRepository = new BorrowerRepository();
    
    private BorrowerService createService() {
        return new BorrowerService(borrowerRepository);
    }
    
    @Test
    public void createdBookHasCorrectAuthorTitleAndCategory() {
        
        BorrowerService service = createService();
        
        Borrower borrower = service.createBorrower("name");
        
        assertEquals("name", borrower.getName());
    }
    
    @Test
    public void repositoryContainsBookAfterCreate() {
        
        BorrowerService service = createService();
        
        Borrower borrower = service.createBorrower("name");
        
        assertEquals(borrower, borrowerRepository.get(borrower.getId()));
    }
    
    @Test
    public void canGetBookAfterCreate() throws LibraryException {
        
        BorrowerService service = createService();
        
        Borrower borrower = service.createBorrower("name");
        
        assertEquals(borrower, service.getBorrower(borrower.getId()));
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanWithInvalidBookIdThrowsException() throws LibraryException {
        
        BorrowerService service = createService();
        
        service.getBorrower(-1);
    }

}
