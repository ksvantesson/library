package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.LibraryPolicy;
import com.cybercom.library.models.Book;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.models.Loan;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.library.repositories.BorrowerRepository;
import com.cybercom.library.repositories.LoanRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import static org.junit.Assert.*;
import org.junit.Test;

public class LoanServiceTest implements LibraryPolicy {
    
    private BookService bookService = new BookService(new BookRepository());
    private BorrowerService borrowerService = new BorrowerService(new BorrowerRepository());
    private LoanRepository loans = new LoanRepository();
    private Book aBook;
    private Borrower aBorrower;
    private Calendar dueDate = new GregorianCalendar(2016, 3, 10);

    public LoanServiceTest() {
        
        aBook = setupBook();
        aBorrower = setupBorrower();
    }

    @Override
    public int getMaxNumBorrowedBooks() {
        return 3;
    }

    @Override
    public Calendar calculateDueDate() {
        return dueDate;
    }
    
    private LoanService createService() {
        return new LoanService(bookService, borrowerService, loans, this);
    }
    
    private Book setupBook() {
        
        return bookService.createBook("", "", "");
    }

    private Borrower setupBorrower() {

        return borrowerService.createBorrower("");
    }

    private void setupOverdueLoan(LoanService service, Borrower borrower) throws LibraryException {
        
        Book overdueBook = setupBook();
        Calendar yesterday = new GregorianCalendar();
        
        yesterday.add(Calendar.DAY_OF_MONTH, -1);
        dueDate = yesterday;
        
        service.createLoan(overdueBook.getId(), borrower.getId());
    }
    
    @Test
    public void createdLoanHasCorrectBook() throws LibraryException {
        
        Book book = setupBook();
        
        LoanService service = createService();
        
        Loan loan = service.createLoan(book.getId(), aBorrower.getId());
        
        assertEquals(book, loan.getBook());
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanWithInvalidBookIdThrowsException() throws LibraryException {
        
        LoanService service = createService();
        
        service.createLoan(-1, aBorrower.getId());
    }
    
    @Test
    public void createdLoanHasCorrectBorrower() throws LibraryException {
        
        Borrower borrower = setupBorrower();
        
        LoanService service = createService();
        
        Loan loan = service.createLoan(aBook.getId(), borrower.getId());
        
        assertEquals(borrower, loan.getBorrower());
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanWithInvalidBorrowerIdThrowsException() throws LibraryException {
        
        LoanService service = createService();
        
        service.createLoan(aBook.getId(), -1);
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanThrowsExceptionIfBorrowerHasBorrowedTooManyBooks() throws LibraryException {
        
        LoanService service = createService();
        
        Borrower borrower = setupBorrower();
        
        for (int i = 0; i <= getMaxNumBorrowedBooks(); i++) {
            
            Book book = bookService.createBook("", "", "");
            
            service.createLoan(book.getId(), borrower.getId());
        }
    }
    
    @Test
    public void loanGetsDueDateAccordingToPolicy() throws LibraryException {
        
        Book book = setupBook();
        Borrower borrower = setupBorrower();
        
        LoanService service = createService();
        
        Loan loan = service.createLoan(book.getId(), borrower.getId());
        
        assertEquals(dueDate, loan.getDueDate());
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanThrowsExceptionIfBorrowerHasOverdueBook() throws LibraryException {
        
        Borrower borrower = setupBorrower();
        
        LoanService service = createService();
        
        setupOverdueLoan(service, borrower);
        
        service.createLoan(setupBook().getId(), borrower.getId());
    }
}
