package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.models.Book;
import com.cybercom.library.repositories.BookRepository;
import static org.junit.Assert.*;
import org.junit.Test;

public class BookServiceTest {
    
    private BookRepository bookRepository = new BookRepository();
    
    private BookService createService() {
        return new BookService(bookRepository);
    }
    
    @Test
    public void createdBookHasCorrectAuthorTitleAndCategory() {
        
        BookService service = createService();
        
        Book book = service.createBook("author", "title", "category");
        
        assertEquals("author", book.getAuthor());
        assertEquals("title", book.getTitle());
        assertEquals("category", book.getCategory());
    }
    
    @Test
    public void repositoryContainsBookAfterCreate() {
        
        BookService service = createService();
        
        Book book = service.createBook("author", "title", "category");
        
        assertEquals(book, bookRepository.get(book.getId()));
    }
    
    @Test
    public void canGetBookAfterCreate() throws LibraryException {
        
        BookService service = createService();
        
        Book book = service.createBook("author", "title", "category");
        
        assertEquals(book, service.getBook(book.getId()));
    }
    
    @Test(expected = LibraryException.class)
    public void createLoanWithInvalidBookIdThrowsException() throws LibraryException {
        
        BookService service = createService();
        
        service.getBook(-1);
    }
}
