package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.repositories.BorrowerRepository;

public class BorrowerService {
    
    private BorrowerRepository borrowers;
    
    public BorrowerService(BorrowerRepository borrowers) {
        
        this.borrowers = borrowers;        
    }
    
    public Borrower createBorrower(String name) {
        
        Borrower borrower = new Borrower();
        
        borrower.setName(name);
        
        borrowers.add(borrower);
        
        return borrower;
    }
    
    public Borrower getBorrower(int id) throws LibraryException {
        
        Borrower borrower = borrowers.get(id);
        
        if (borrower != null)
            return borrower;
        
        throw new LibraryException(String.format("Det finns ingen låntagare med ID #%d", id));
    }
}
