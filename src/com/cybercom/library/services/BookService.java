package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.models.Book;
import com.cybercom.library.repositories.BookRepository;

public class BookService {

    private BookRepository books;
    
    public BookService(BookRepository books) {
        this.books = books;
    }

    public Book createBook(String author, String title, String category) {
        
        Book book = new Book();
        
        book.setAuthor(author);
        book.setTitle(title);
        book.setCategory(category);
        
        books.add(book);
        
        return book;
    }

    public Book getBook(int id) throws LibraryException {
        
        Book book = books.get(id);
        
        if (book == null)
            throw new LibraryException(String.format("Det finns ingen bok med ID #%d", id));
        
        return book;
    }
}
