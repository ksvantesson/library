package com.cybercom.library.services;

import com.cybercom.library.LibraryException;
import com.cybercom.library.LibraryPolicy;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.models.Loan;
import com.cybercom.library.repositories.LoanRepository;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

public class LoanService {
    
    private BookService bookService;
    private BorrowerService borrowerService;
    private LoanRepository loanRepository;
    private LibraryPolicy policy;

    public LoanService(BookService bookService, BorrowerService borrowerService, LoanRepository loanRepository, LibraryPolicy policy) {
        
        this.bookService = bookService;
        this.borrowerService = borrowerService;
        this.loanRepository = loanRepository;
        this.policy = policy;
    }

    public Loan createLoan(int bookId, int borrowerId) throws LibraryException {
        
        Borrower borrower = borrowerService.getBorrower(borrowerId);
        
        verifyMayBorrow(borrower);
        
        Loan loan = new Loan(bookService.getBook(bookId), borrower, policy.calculateDueDate());
        
        loanRepository.add(loan);
        
        return loan;
    }

    private void verifyMayBorrow(Borrower borrower) throws LibraryException {
        
        List<Loan> loans = loanRepository.find(borrower);
        
        verifyNumberOfBorrowedBooks(loans);
        verifyDueDates(loans);
    }

    private void verifyNumberOfBorrowedBooks(List<Loan> loans) throws LibraryException {

        if (loans.size() >= policy.getMaxNumBorrowedBooks())
            throw new LibraryException(String.format("Låntagaren har redan lånat %d böcker.", policy.getMaxNumBorrowedBooks()));
    }

    private void verifyDueDates(List<Loan> loans) throws LibraryException {

        GregorianCalendar now = new GregorianCalendar();
        
        for (Loan loan : loans) {
            
            if (now.after(loan.getDueDate())) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                throw new LibraryException(String.format("Låntagaren har ett lån som skulle varit återlämnat %s.", dateFormat.format(loan.getDueDate().getTime())));
            }
        }
    }
}
