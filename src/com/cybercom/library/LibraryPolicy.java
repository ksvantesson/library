package com.cybercom.library;

import java.util.Calendar;

public interface LibraryPolicy {
    
    int getMaxNumBorrowedBooks();
    
    Calendar calculateDueDate();

}
