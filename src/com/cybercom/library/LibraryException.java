package com.cybercom.library;

public class LibraryException extends Exception {

    public LibraryException(String message) {
        super(message);
    }

}
