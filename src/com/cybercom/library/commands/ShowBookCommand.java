package com.cybercom.library.commands;

import com.cybercom.library.models.Book;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class ShowBookCommand extends Command {
    
    private Prompt prompt;
    private BookRepository books;
    
    public ShowBookCommand(Prompt prompt, BookRepository books) {
        
        super("Visa bok");
        
        this.prompt = prompt;
        this.books = books;
    }

    @Override
    protected boolean onExecute() {
        
        int bookId = prompt.getInt("Ange bok-ID: #");
        Book book = books.get(bookId);
        
        if (book != null) {
            System.out.println("Författare: " + book.getAuthor());
            System.out.println("Titel:      " + book.getTitle());
            System.out.println("Kategori:   " + book.getCategory());
            
            /*Loan loan = loans.find(book);
            
            if (loan != null) {
                System.out.println(String.format("Lånad av %s", loan.getBorrower().getName()));
            }*/
        }
        else {
            System.out.println("Det finns ingen bok med ID #" + bookId);
        }
        
        return true;
    }
}
