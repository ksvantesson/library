package com.cybercom.library.commands;

import com.cybercom.library.LibraryException;
import com.cybercom.library.services.LoanService;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class RegisterLoanCommand extends Command {
    
    private Prompt prompt;
    private LoanService loanService;
    
    public RegisterLoanCommand(Prompt prompt, LoanService loanService) {
        
        super("Registrera lån");
        
        this.prompt = prompt;
        this.loanService = loanService;
    }

    @Override
    protected boolean onExecute() {
        
        int bookId = prompt.getInt("Ange bok-ID: #");
        int borrowerId = prompt.getInt("Ange låntagar-ID: #");
        
        try {
            
            loanService.createLoan(bookId, borrowerId);
            System.out.println();
            System.out.println("Lånet är nu registrerat.");
        }
        catch (LibraryException ex) {
            
            System.out.println();
            System.out.println("Lånet ej beviljat:");
            System.out.println(ex.getMessage());
        }
        
        return true;
    }
}
