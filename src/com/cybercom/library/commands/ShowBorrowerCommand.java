package com.cybercom.library.commands;

import com.cybercom.library.models.Borrower;
import com.cybercom.library.models.Loan;
import com.cybercom.library.repositories.BorrowerRepository;
import com.cybercom.library.repositories.LoanRepository;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class ShowBorrowerCommand extends Command {
    
    private Prompt prompt;
    private BorrowerRepository borrowers;
    private LoanRepository loans;
    
    public ShowBorrowerCommand(Prompt prompt, BorrowerRepository borrowers, LoanRepository loans) {
        
        super("Visa låntagare");
        
        this.prompt = prompt;
        this.borrowers = borrowers;
        this.loans = loans;
    }

    @Override
    protected boolean onExecute() {
        
        int id = prompt.getInt("Ange låntagar-ID: #");
        Borrower borrower = borrowers.get(id);
        
        if (borrower != null) {
            System.out.println("Namn: " + borrower.getName());
            
            for (Loan loan : loans.find(borrower)) {
                System.out.println(String.format("%s %s", loan.getBook().getAuthor(), loan.getBook().getTitle()));
            }
        }
        else {
            System.out.println("Det finns ingen låntagare med ID #" + id);
        }
        
        return true;
    }
}
