package com.cybercom.library.commands;

import com.cybercom.library.models.Book;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.util.Command;
import com.cybercom.util.Menu;
import com.cybercom.util.Prompt;
import java.util.List;
import java.util.function.Function;

public class SearchBooks2Command extends Command {
    
    private Prompt prompt;
    private BookRepository books;
    
    public SearchBooks2Command(Prompt prompt, BookRepository books) {
        
        super("Sök böcker");
        
        this.prompt = prompt;
        this.books = books;
    }

    @Override
    protected boolean onExecute() {
        
        Menu menu = new Menu(prompt,
                        new SearchCommand("Sök titel", books, book -> book.getTitle()),
                        new SearchCommand("Sök författare", books, book -> book.getAuthor()),
                        new SearchCommand("Sök kategori", books, book -> book.getCategory()));
        
        menu.run();
        
        return true;
    }
    
    private final class SearchCommand extends Command {
        
        private BookRepository books;
        private Function<Book, String> map;
        
        public SearchCommand(String searchName, BookRepository books, Function<Book, String> map) {
            
            super(searchName);
            
            this.map = map;
            this.books = books;
        }

        @Override
        protected boolean onExecute() {
            
            String text = prompt.getString("Ange sökvillkor: ").toLowerCase();
            
            for (Book book : searchBooks(text)) {
                System.out.println(String.format("%s %s %s", book.getAuthor(), book.getTitle(), book.getCategory()));
            }
            
            return false;
        }

        private List<Book> searchBooks(String text) {
            return books.search(book -> map.apply(book).toLowerCase().contains(text));
        }
    }

}
