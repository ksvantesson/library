package com.cybercom.library.commands;

import com.cybercom.library.models.Borrower;
import com.cybercom.library.services.BorrowerService;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class RegisterBorrowerCommand extends Command {
    
    private Prompt prompt;
    private BorrowerService borrowerService;
    
    public RegisterBorrowerCommand(Prompt prompt, BorrowerService borrowerService) {
        
        super("Registrera låntagare");
        
        this.prompt = prompt;
        this.borrowerService = borrowerService;
    }

    @Override
    protected boolean onExecute() {
        
        Borrower borrower = borrowerService.createBorrower(prompt.getString("Ange namn: "));
        
        System.out.println();
        System.out.println(String.format("Låntagaren är nu registrerad (ID #%d).", borrower.getId()));
        
        return true;
    }
}
