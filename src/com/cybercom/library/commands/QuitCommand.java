package com.cybercom.library.commands;

import com.cybercom.util.Command;

public class QuitCommand extends Command {
    
    public QuitCommand() {
        super("Avsluta");
    }

    @Override
    protected boolean onExecute() {
        return false;
    }

}
