package com.cybercom.library.commands;

import com.cybercom.library.models.Book;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.util.Command;
import com.cybercom.util.Menu;
import com.cybercom.util.Prompt;
import java.util.ArrayList;
import java.util.List;

public class SearchBooksCommand extends Command {
    
    private Prompt prompt;
    private BookRepository books;
    
    public SearchBooksCommand(Prompt prompt, BookRepository books) {
        
        super("Sök böcker");
        
        this.prompt = prompt;
        this.books = books;
    }

    @Override
    protected boolean onExecute() {
        
        Menu menu = new Menu(prompt,
                        new SearchCommand("Sök titel", books, (Book book, String searchText) -> book.getTitle().toLowerCase().contains(searchText)),
                        new SearchCommand("Sök författare", books, (Book book, String searchText) -> book.getAuthor().toLowerCase().contains(searchText)),
                        new SearchCommand("Sök kategori", books, (Book book, String searchText) -> book.getCategory().toLowerCase().contains(searchText)));
        
        menu.run();

        
        return true;
    }
    
    private interface BookMatcher {
        
        boolean matches(Book book, String searchText);
    }
    
    private final class SearchCommand extends Command {
        
        private BookRepository books;
        private BookMatcher matcher;
        
        public SearchCommand(String searchName, BookRepository books, BookMatcher matcher) {
            
            super(searchName);
            
            this.books = books;
            this.matcher = matcher;
        }

        @Override
        protected boolean onExecute() {
            
            String text = prompt.getString("Ange sökvillkor: ").toLowerCase();
            
            for (Book book : searchBooks(text)) {
                System.out.println(String.format("%s %s %s", book.getAuthor(), book.getTitle(), book.getCategory()));
            }
            
            return false;
        }

        private List<Book> searchBooks(String text) {
            
            ArrayList<Book> result = new ArrayList<>();
            
            for (Book book : books.all()) {
                
                if (matcher.matches(book, text))
                    result.add(book);
            }
            
            return result;
        }
    }
}
