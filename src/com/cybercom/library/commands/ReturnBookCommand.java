package com.cybercom.library.commands;

import com.cybercom.library.models.Loan;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.library.repositories.LoanRepository;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class ReturnBookCommand extends Command {
    
    private Prompt prompt;
    private BookRepository books;
    private LoanRepository loans;
    
    public ReturnBookCommand(Prompt prompt, BookRepository books, LoanRepository loans) {
        
        super("Återlämna bok");
        
        this.prompt = prompt;
        this.books = books;
        this.loans = loans;
    }

    @Override
    protected boolean onExecute() {
        
        int bookId = prompt.getInt("Ange bok-ID: #");
        
        Loan loan = loans.find(books.get(bookId));
        
        loans.remove(loan);
        
        return true;
    }
}
