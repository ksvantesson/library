package com.cybercom.library.commands;

import com.cybercom.library.models.Book;
import com.cybercom.library.services.BookService;
import com.cybercom.util.Command;
import com.cybercom.util.Prompt;

public class RegisterBookCommand extends Command {
    
    private Prompt prompt;
    private BookService bookService;
    
    public RegisterBookCommand(Prompt prompt, BookService bookService) {
        
        super("Registrera bok");
        this.prompt = prompt;
        this.bookService = bookService;
    }

    @Override
    protected boolean onExecute() {
        
        String author = prompt.getString("Ange författare: ");
        String title = prompt.getString("Ange titel: ");
        String category = prompt.getString("Ange kategori: ");
        
        Book book = bookService.createBook(author, title, category);
        
        System.out.println();
        System.out.println(String.format("Boken är nu registrerad. Den har ID #%d.", book.getId()));
        
        return true;
    }
}
