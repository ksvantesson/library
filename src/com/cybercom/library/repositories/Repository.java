package com.cybercom.library.repositories;

import com.cybercom.library.models.Identity;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

abstract public class Repository<E extends Identity> {
    
    private ArrayList<E> entities = new ArrayList<>();
    private int nextId = 1;

    public List<E> all() {
        return entities;
    }

    public void add(E entity) {
        entities.add(entity);
        entity.setId(nextId++);
    }

    public E get(int id) {
        
        for (E entity : entities) {
            if (entity.getId() == id)
                return entity;
        }

        return null;
    }

    public int getNextId() {
        return nextId;
    }

    public void setNextId(int nextId) {
        this.nextId = nextId;
    }

    public ArrayList<E> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<E> entities) {
        this.entities = entities;
    }
    
    public List<E> search(Predicate<E> predicate) {
        return entities.stream().filter(predicate).collect(Collectors.toList());
    }
}
