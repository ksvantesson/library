package com.cybercom.library.repositories;

import com.cybercom.library.models.Book;
import com.cybercom.library.models.Borrower;
import com.cybercom.library.models.Loan;
import java.util.ArrayList;
import java.util.List;

public class LoanRepository {
    
    private ArrayList<Loan> loans = new ArrayList<>();
    
    public List<Loan> all() {
        return loans;
    }

    public void add(Loan loan) {
        loans.add(loan);
    }

    public Loan find(Book book) {
        
        for (Loan loan : loans) {
            if (loan.getBook() == book)
                return loan;
        }
        
        return null;
    }
    
    public List<Loan> find(Borrower borrower) {
        ArrayList<Loan> result = new ArrayList<>();
        
        for (Loan loan : loans) {
            if (loan.getBorrower() == borrower)
                result.add(loan);
        }
        
        return result;
    }

    public void remove(Loan loan) {
        loans.remove(loan);
    }
}
