package com.cybercom.library.repositories;

import com.cybercom.library.models.Loan;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class LoanRepositoryJsonPersister {
    
    private ObjectMapper mapper;
    
    public LoanRepositoryJsonPersister(ObjectMapper mapper) {
        
        this.mapper = mapper;
    }
    
    public LoanRepository load(BookRepository books, BorrowerRepository borrowers) throws IOException {
        
        LoanRepository loans = new LoanRepository();
        
        for (LoanPersistModel loan : mapper.readValue(new File("loans.json"), LoanPersistModel[].class)) {
            
            loans.add(toLoan(loan, books, borrowers));
        }
        
        return loans;
    }

    private static Loan toLoan(LoanPersistModel loan, BookRepository books, BorrowerRepository borrowers) {
        return new Loan(books.get(loan.getBookId()), borrowers.get(loan.getBorrowerId()), loan.getDueDate());
    }
    
    public void save(LoanRepository loans) throws IOException {

        mapper.writeValue(new File("loans.json"), loans.all().stream().map(loan -> toModel(loan)).toArray());
    }

    private LoanPersistModel toModel(Loan loan) {
        
        LoanPersistModel model = new LoanPersistModel();
        
        model.setBookId(loan.getBook().getId());
        model.setBorrowerId(loan.getBorrower().getId());
        model.setDueDate(loan.getDueDate());
        
        return model;
    }
}
    
class LoanPersistModel {
    
    private int bookId;
    private int borrowerId;
    private Calendar dueDate;

    public LoanPersistModel() {
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(int borrowerId) {
        this.borrowerId = borrowerId;
    }
    
    public Calendar getDueDate() {
        return dueDate;
    }
    
    public void setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
    }
}
