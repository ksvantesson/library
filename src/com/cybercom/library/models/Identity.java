package com.cybercom.library.models;

public interface Identity {
    
    int getId();
    void setId(int id);

}
