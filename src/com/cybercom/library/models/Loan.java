package com.cybercom.library.models;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Loan {
    
    private Book book;
    private Borrower borrower;
    private Calendar dueDate;
    
    public Loan(Book book, Borrower borrower, Calendar dueDate) {
        this.book = book;
        this.borrower = borrower;
        this.dueDate = dueDate;
    }

    public Book getBook() {
        return book;
    }

    public Borrower getBorrower() {
        return borrower;
    }
    
    public Calendar getDueDate() {
        return dueDate;
    }
}
