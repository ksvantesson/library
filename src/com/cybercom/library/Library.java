package com.cybercom.library;

import com.cybercom.library.commands.QuitCommand;
import com.cybercom.library.commands.RegisterBookCommand;
import com.cybercom.library.commands.RegisterBorrowerCommand;
import com.cybercom.library.commands.RegisterLoanCommand;
import com.cybercom.library.commands.ReturnBookCommand;
import com.cybercom.library.commands.SearchBooksCommand;
import com.cybercom.library.commands.ShowBookCommand;
import com.cybercom.library.commands.ShowBorrowerCommand;
import com.cybercom.library.repositories.BookRepository;
import com.cybercom.library.repositories.BorrowerRepository;
import com.cybercom.library.repositories.LoanRepository;
import com.cybercom.library.repositories.LoanRepositoryJsonPersister;
import com.cybercom.library.services.BookService;
import com.cybercom.library.services.BorrowerService;
import com.cybercom.library.services.LoanService;
import com.cybercom.util.Menu;
import com.cybercom.util.Prompt;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Library implements LibraryPolicy { 
    
    private BookRepository books = new BookRepository();
    private BorrowerRepository borrowers = new BorrowerRepository();
    private LoanRepository loans = new LoanRepository();
    Prompt prompt = new Prompt();

    public static void main(String[] args) {
        
        Library library = new Library();
        
        library.run();
    }
    
    private void run() {
        
        loadRepositories();
        
        BookService bookService = new BookService(books);
        BorrowerService borrowerService = new BorrowerService(borrowers);
        LoanService loanService = new LoanService(bookService, borrowerService, loans, this);
        
        Menu menu = new Menu(prompt,
                new RegisterBookCommand(prompt, bookService),
                new RegisterBorrowerCommand(prompt, borrowerService),
                new ShowBookCommand(prompt, books),
                new ShowBorrowerCommand(prompt, borrowers, loans),
                new RegisterLoanCommand(prompt, loanService),
                new ReturnBookCommand(prompt, books, loans),
                new SearchBooksCommand(prompt, books),
                new QuitCommand());
        
        menu.run();
        
        saveRepositories();
    }

    private void loadRepositories() {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            
            books = mapper.readValue(new File("books.json"), BookRepository.class);
            borrowers = mapper.readValue(new File("borrowers.json"), BorrowerRepository.class);
            
            LoanRepositoryJsonPersister loanPersister = new LoanRepositoryJsonPersister(mapper);
            
            loans = loanPersister.load(books, borrowers);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveRepositories() {
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            
            mapper.writeValue(new File("books.json"), books);
            mapper.writeValue(new File("borrowers.json"), borrowers);
            
            LoanRepositoryJsonPersister loanPersister = new LoanRepositoryJsonPersister(mapper);
            
            loanPersister.save(loans);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getMaxNumBorrowedBooks() {
        return 5;
    }

    @Override
    public Calendar calculateDueDate() {
        
        Calendar dueDate = new GregorianCalendar();
        
        dueDate.add(Calendar.DAY_OF_MONTH, 14);
        
        return dueDate;
    }
}
