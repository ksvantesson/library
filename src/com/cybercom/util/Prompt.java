package com.cybercom.util;

import java.util.Scanner;

public class Prompt {
    
    private Scanner scanner = new Scanner(System.in, "ISO-8859-1");
    
    public String getString(String question) {
        
        System.out.print(question);
        
        return scanner.nextLine();
    }

    public int getInt(String question) {
        
        while (true) {
            try {
                String value = getString(question);
                
                return Integer.parseInt(value);
            }
            catch (Exception e) {
                System.out.println("Du måste ange ett heltal.");
            }
        }
    }

}
