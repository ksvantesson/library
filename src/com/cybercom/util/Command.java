package com.cybercom.util;

abstract public class Command {

    private String name;
    
    protected Command(String name) {
        this.name = name;
    }
    
    public boolean execute() {
        
        System.out.println("--- " + name + " ---");
        System.out.println("");
        
        boolean result = onExecute();
        
        System.out.println("");
        
        return result;
    }
    
    abstract protected boolean onExecute();
    
    public String getName() {
        return name;
    }
}
