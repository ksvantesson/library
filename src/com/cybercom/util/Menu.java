package com.cybercom.util;

public class Menu {
    
    private Prompt prompt;
    private Command[] menuItems;
    
    public Menu(Prompt prompt, Command... commands) {
        
        this.prompt = prompt;
        this.menuItems = commands;
    }
    
    public void run() {
        
        while (true) {
            showMenu();

            Command menuItem = promptMenuItem();

            if (!menuItem.execute())
                return;
        }
    }

    private void showMenu() {
        
        for (int i = 0; i < menuItems.length; i++) {
            System.out.println(String.format("%d - %s", i + 1, menuItems[i].getName()));
        }
    }

    private Command promptMenuItem() {
        int index = prompt.getInt("Vad vill du göra? ") - 1;
        
        if (0 <= index && index < menuItems.length)
            return menuItems[index];
        
        return new InvalidIndexCommand();
    }
    
    private class InvalidIndexCommand extends Command {
        
        public InvalidIndexCommand() {
            super("Ogiltig operation");
        }

        @Override
        protected boolean onExecute() {
            return true;
        }
    }
}
